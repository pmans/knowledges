<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Explicit extends Model
{
    protected $table = 'explicits';
    protected $fillable = ['title','description','url','id_user'];

    public function user()
     {
         return $this->belongsTo('App\User', 'id_user');
     }
}
