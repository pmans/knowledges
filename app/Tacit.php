<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tacit extends Model
{
    protected $table = 'tacits';
    protected $fillable = ['complain','action','information','status','id_user'];
}
