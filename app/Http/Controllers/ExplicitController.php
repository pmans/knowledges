<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\User;
use App\UserRole;


class ExplicitController extends Controller
{

    private $explicits, $users;

    public function __construct()
    {
        $this->explicits = Explicit::all();
        $this->userroles = UserRole::all();

    }

    // public function index()
    // {
    //     // $user = \App\User::count();
    //     // $explicit = \App\Explicit::count();
    //     // $tacit = \App\Tacit::count();

    //     // $explicits = \App\Explicit::all();
    //     // return view('pakar/home', compact('explicits','explicit','tacit'));

    // }

    public function add()
    {
        $userroles = \App\UserRole::all();
        return view('pakar/add-explicit',compact('userroles'));
    }

    public function create()
    {
        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required|min:100',
            'url' => 'required',
        ]);

        Explicit::create([
            'title' => request('title'),
            'description' => request('description'),
            'url' => request('url'),
            'id_user' => request('id_user')
            ]);
        return redirect('pakar/add-explicit')->withInfo('Explicit Successfully Add!!!');
    }

    public function show()
    {
        $userroles = \App\UserRole::all();
        $explicits = $this->explicits;
        $explicits = \App\Explicit::all();
        return view('pakar.shows-my-explicit', compact('explicits','userroles'));        
       
    }

    public function detail($id)
    {   

        $explicits = $this->explicits;
        $userroles = \App\UserRole::all();
        $explicits = \App\Explicit::all();
        $users     = \App\User::find($id);
        $explicits = Explicit::find($id);

        return view('pakar.detail-show-explicit', [
            'title'=>$explicits->title,
            'description'=>$explicits->description,
            'url'=>$explicits->url,
            'created_at'=>$explicits->created_at,
            'id_user'=>$explicits->id_user,
            'id'=>$explicits->id

            ] );
    }
}