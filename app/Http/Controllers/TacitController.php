<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tacit;
use App\UserRole;


class TacitController extends Controller
{

    public function add()

    {
        $this->userroles = UserRole::all();
        $userroles = \App\UserRole::all();
        return view('pakar/add-tacit',compact('userroles'));
    }

    public function create()

    {
        $this->validate(request(), [
            'complain'      => 'required|min:10',
            'action'        => 'required|min:10',
            'information'   => 'required',

        ]);

        Tacit::create([
            'complain'         => request('complain'),
            'action'   => request('action'),
            'information'           => request('information'),
            'status'       => request('status'),
            'id_user'       => request('id_user')

            ]);
        return redirect('pakar/add-tacit')->withInfo('Tacit Successfully Add!!!');
    }

    public function show()
    {
        $this->userroles = UserRole::all();
        $userroles = \App\UserRole::all();
        $tacits = \App\Tacit::all();
        return view('pakar.shows-tacit', compact('tacits','userroles'));        
    }

    public function delete($id)
    {
        $tacits = Tacit::find($id)->delete();

        return redirect('pakar/shows-tacit')->withInfo('Tacit Successfully Delete!!!');

    }

}
