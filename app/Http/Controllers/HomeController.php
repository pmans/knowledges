<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\User;
use App\UserRole;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userroles;

    public function __construct()
    {
        $this->middleware('auth');
        $this->userroles = UserRole::all();
    }

    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \App\User::count();
        $explicit = \App\Explicit::count();
        $tacit = \App\Tacit::count();

        $userroles = \App\UserRole::all();
        $explicits = \App\Explicit::all();

        return view('home', compact('userroles','explicits','explicit','tacit'));


    }

}
