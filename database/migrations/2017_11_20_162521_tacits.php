<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tacits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('tacits', function (Blueprint $table) {
            $table->increments('id');
            $table->text('complain');
            $table->text('action');
            $table->string('information');
            $table->enum('status', ['Pending', 'End']);
            $table->integer('id_user')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
