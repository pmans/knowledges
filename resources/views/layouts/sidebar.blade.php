


    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

        <ul class="nav menu">
            <li class="list-group-item">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" ></span> {{ Auth::user()->name }} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <span class="glyphicon glyphicon-log-out"></span> Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}
                    </form>
                    </li>
                </ul>
            </li>
        </ul>

           
          
        <ul class="nav menu">
            <li><a href="/home"><span class="glyphicon glyphicon-dashboard"></span> DASBOARD </a></li>


            <li role="presentation" class="divider"></li>
        </ul>



        <div class="attribution">KMS Beta 0.0</a></div>
    </div>
