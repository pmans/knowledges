@extends('layouts.master')
@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div><!--/.row-->
   
    <div class="row">
        <div class="col-lg-12">     
            <div class="panel panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-dashboard"></span>DASHBOARD</div>
                <div class="panel-body">
                <div class="row">

            <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="panel panel-blue panel-widget ">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left">
                            <em class="glyphicon glyphicon-user glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <b>{{ Auth::user()->username }}</b>
                            <div class="text-muted"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="panel panel-blue panel-widget">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left">
                            <em class="glyphicon glyphicon-book glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <div class="large"></div>
                            <div class="text-muted">Explicits</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="panel panel-blue panel-widget">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left">
                            <em class="glyphicon glyphicon-list-alt glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <div class="large"></div>
                            <div class="text-muted">Tacits</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                </div>
            </div>
        </div>
    </div>

        
        

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-book"></span>EXPLICIT KNOWLEDGES DATA</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="table">
                            <table width="100%" class="table table-hover" id="table">

                                <thead>
                                    <tr>
                                        <th><h4><center>#</center></h4></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                    
                                <tbody>
                                    @foreach ($explicits as $explicit)  
                                    <?php $view = $explicit->description ;
                                        $content = substr($view ,0 ,200); ?>
                                    <tr>
                                        <td><h4>{{ $loop->index+1 }}</h4></td>
                                        <td>

                                        <div class="post-detail">
                                            <h4><a href="{{ route('detail-explicit', $explicit->id) }}"><b>{{ $explicit->title }}</b></a></h4>
                                            <ul class="list-inline">
                                                <li>
                                                    <i><span class="label label-info"><span class="glyphicon glyphicon-calendar"></span> {{ $explicit->created_at }}</span>

                                                    <span class="label label-info"><span class="glyphicon glyphicon-time"></span> {{ $explicit->created_at->diffForHumans() }}</span>

                                                    <span class="label label-info"> <span class="glyphicon glyphicon-user"></span> Posting by {{ $explicit->user->name }}</i></span>
                                                </li>
                                            </ul>
                                        </div>


                                        <?php echo $content; ?>...<br>
                                        <a href="{{ route('detail-explicit', $explicit->id) }}"><button type="submit" class="btn btn-info btn-fill btn-xs"> Read More...</button></a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
      
@endsection