@extends('layouts.master')
@section('content')
   
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><span class="glyphicon glyphicon-book"></span>FORM ADD EXPLICIT</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form action="{{ route('post-explicit') }}" method="POST" role="form" enctype="multipart/form-data">
                             {{csrf_field()}}        

                                <div class="form-group has-feedback{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label>TITLE</label>
                                    <input name="title" class="form-control" placeholder="Title" value="{{ old('title') }}">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <p><b>{{ $errors->first('title') }}</b></p>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label>DESCRIPTION</label>
                                    <textarea name="description" id="ckeditor" required>{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <p><b>{{ $errors->first('description') }}</b></p>
                                        </span>
                                    @endif
                                </div>
                
                                <div class="form-group has-feedback{{ $errors->has('url') ? ' has-error' : '' }}">
                                    <label>URL</label>
                                    <input name="url" class="form-control" placeholder="https://projects.co.id/" value="{{ old('url') }}">
                                    @if ($errors->has('url'))
                                        <span class="help-block">
                                            <p><b>{{ $errors->first('url') }}</b></p>
                                        </span>
                                    @endif
                                </div>

                                 <div class="form-group">
                                    <input name="id_user" class="form-control" type="hidden" value="{{ Auth::user()->id }}">
                                </div>

                             <!--    <div class="form-group">
                                    <label>IMAGE</label>
                                    <input name="image" type="file" class="form-control">
                                </div>
 -->
                 <!--                <div class="form-group">
                                    <label>IMAGES</label>
                                    <div class="input-group image-preview">                                   
                                        <span class="input-group-btn">                                     
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                <span class="glyphicon glyphicon-remove"></span> Clear
                                        </button>
                                      
                                        <div class="btn btn-default image-preview-input">
                                            <span class="image-preview-input-title"></span>
                                            <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/>
                                        </div>
                                        </span>
                                    </div>
                                </div> -->
                        
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Add Explicit</button>
                
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection