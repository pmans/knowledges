
@extends('layouts.master')

@section('content')
   
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span>FORM ADD TACIT</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form action="{{ route('post-tacit') }}" method="POST" role="form" enctype="multipart/form-data">
                             {{csrf_field()}}        

                                <div class="form-group has-feedback{{ $errors->has('complain') ? ' has-error' : '' }}">
                                    <label>COMPLAIN</label>
                                    <textarea name="complain" class="form-control" id="exampleFormControlTextarea1" rows="6">{{ old('complain') }}</textarea>
                                    @if ($errors->has('complain'))
                                        <span class="help-block">
                                            <p><b>{{ $errors->first('complain') }}</b></p>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback{{ $errors->has('action') ? ' has-error' : '' }}">
                                    <label>ACTION</label>
                                    <textarea name="action" class="form-control" id="exampleFormControlTextarea1" rows="6">{{ old('action') }}</textarea>
                                    @if ($errors->has('action'))
                                        <span class="help-block">
                                            <p><b>{{ $errors->first('action') }}</b></p>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback{{ $errors->has('information') ? ' has-error' : '' }}">
                                    <label>INFORMATION</label>
                                    <input name="information" class="form-control" placeholder="Information" value="{{ old('information') }}">
                                    @if ($errors->has('information'))
                                        <span class="help-block">
                                            <p><b>{{ $errors->first('information') }}</b></p>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>STATUS</label>
                                    <select name="status" class="form-control form-control-sm">
                                    <option value="Pending">PENDING</option>
                                    <option value="End">END</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input name="id_user" class="form-control" type="hidden" value="{{ Auth::user()->id }}">
                                </div>

                                <button type="submit" class="btn btn-info btn-fill btn-wd">Add Tacit </button>
                
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
