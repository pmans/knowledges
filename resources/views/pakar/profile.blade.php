@extends('layouts.master')
@section('content')

</style>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6"> 
            <div class="panel panel-info">
                <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>{{ Auth::user()->name }}</div>
                <div class="panel-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><b>NAME</b></td>
                            <td class="info">{{ Auth::user()->name }}</td>
                        </tr>
                        <tr>
                            <td><b>USERNAME</b></td>
                            <td class="info">{{ Auth::user()->username }}</td>
                        </tr>
                        <tr>
                            <td><b>EMAIL</b></td>
                            <td class="info">{{ Auth::user()->email }}</td>
                        </tr>
                        <tr>
                            <td><b>POSITION</b></td>
                            <td class="info">{{ Auth::user()->position }}</td>
                        </tr>
                        <tr>
                            <td><b>Home Address</b></td>
                            <td class="info">Kathmandu,Nepal</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>

        <div class="col-md-6"> 
            <div class="panel panel-info">
                <div class="panel-heading"><span class="glyphicon glyphicon-edit"></span>EDIT YOUR PROFILE</div>
                <div class="panel-body">
                <form class="form-horizontal" action="{{ route('post-explicit') }}" method="POST" role="form" enctype="multipart/form-data">
                            {{csrf_field()}}        

                    <div class="form-group">
                        <label class="col-sm-2" for="name">NAME</label>
                            <div class="col-sm-10">
                            <input name="name" type="text" class="form-control" value="{{ Auth::user()->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="username">USERNAME</label>
                            <div class="col-sm-10">
                            <input name="username" id="username" type="text" class="form-control" value="{{ Auth::user()->username }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="email">E-MAIL</label>
                            <div class="col-sm-10">
                            <input name="email" id="email" type="text" class="form-control" value="{{ Auth::user()->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="password">PASSWORD</label>
                            <div class="col-sm-10">
                            <input name="password" id="password" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="email">VERIFY</label>
                            <div class="col-sm-10">
                            <input name="email" id="email" type="text" class="form-control">
                        </div>
                    </div>

                </form>
                 </div>
            </div>
        </div>
    </div>
</div>


@endsection