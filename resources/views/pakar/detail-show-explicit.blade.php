@extends('layouts.master')
@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">

                    <div class="panel-heading">
                        <h4 class="panel-title"> {{ $title }}</h4>
                        <h6><ul class="list-inline">
                            <li>
                                <span class="label label-info"><span class="glyphicon glyphicon-calendar"></span>{{ $created_at }}</span>
                                <span class="label label-info"><span class="glyphicon glyphicon-time"></span>{{ $created_at->diffForHumans() }}</span>
                                @if ( $id_user == Auth::user()->id)
                                <span class="label label-info"><span class="glyphicon glyphicon-user"></span>Posting by {{ Auth::user()->name }}</span>
                            </li>
                            @endif
                        </ul></h6>
                     </div> 

                    <div class="panel-body">
                        <div class="col-md-12">
                          <?php echo$description; ?> <p>
                        <div class="alert alert-info">
                            <strong>URL :</strong> <i><a href="{{ $url }}" target="_blank">{{ $url }}</a></i>
                        </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection