<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

use Illuminate\Http\Request;
 Route::get('/', function () {
     return view('welcome');
 });


// Route untuk user karyawan

Route::get('/home', 'HomeController@index')->name('home');
     


// Route untuk user yang admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth','role:admin']], function(){
    Route::get('/', function(){
        $data['users'] = \App\User::whereDoesntHave('roles')->get();
        return view('admin.admin', $data);
    });
});


// Route untuk user yang pakar
Route::group(['prefix' => 'pakar', 'middleware' => ['auth','role:pakar']], function(){
Route::get('/home', 'ExplicitController@index')->name('home');
//PROLFILE
Route::get('profile', 'UserController@index')->name('profile');
//ENDPROFILE
// EXPILCIT
Route::get('/add-explicit', 'ExplicitController@add')->name('add-explicit');
Route::post('/add-explicit', 'ExplicitController@create')->name('post-explicit');
Route::get('/my-explicit', 'ExplicitController@show')->name('shows-my-explicit');
Route::get('/shows-explicit/{id}/detail-shows-explicit', 'ExplicitController@detail')->where('id', '\d+')->name('detail-explicit');
// ENDEXLICIT
// TACIT
Route::get('/add-tacit', 'TacitController@add')->name('add-tacit');
Route::post('/add-tacit', 'TacitController@create')->name('post-tacit');
Route::get('/shows-tacit', 'TacitController@show')->name('shows-tacit');
Route::get('/{id}/delete-tacit', 'TacitController@delete')->name('delete-tacit');
//ENDTACIT


 });



